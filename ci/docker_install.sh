#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer.
apt-get update -yqq
apt-get install \
        git \
        libmcrypt-dev \
        libsqlite3-dev \
        unzip \
        wget \
        -yqq

# Install xdebug.
pecl install xdebug-2.5.0 && docker-php-ext-enable xdebug

# Install mysql driver
# Here, add any other extensions that we need
docker-php-ext-install  \
    mcrypt \
    pdo_mysql \
    pdo_sqlite
