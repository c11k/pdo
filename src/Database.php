<?php
/**
 * PHP Version 7.1
 *
 * @package c11k/pdo
 */
declare(strict_types=1);

namespace C11K\Pdo;

use Exception;
use PDO;

class Database
{
    protected $host;
    protected $user;
    protected $pass;
    protected $databaseName;
    protected $source;
    /** @var \PDO */
    protected $dbHandle;
    protected $error;
    /** @var  \PDOStatement */
    protected $statement;

    public function __construct()
    {
        $this->host = getenv('DB_HOST');
        $this->user = getenv('DB_USER');
        $this->pass = getenv('DB_PASS');
        $this->databaseName = getenv('DB_NAME');
        $this->source = getenv('DB_PREFIX');

        $options = [
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        ];

        $this->dbHandle = new PDO($this->createDSN(), $this->user, $this->pass, $options);
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function createDSN(): string
    {
        switch ($this->source) {
            case 'mysql:':
                $attributes = [
                    'host' => $this->host ?? 'localhost',
                    'port' => getenv('DB_PORT') ?: 3306,
                    'dbname' => $this->databaseName,
                ];
                break;
            case 'sqlite:':
                return 'sqlite:' . $this->databaseName;
                break;
            case 'sqlite::memory:':
                return 'sqlite::memory:';
                break;
            case 'pgsql:':
                $attributes = [
                    'host' => $this->host ?? 'localhost',
                    'port' => getenv('DB_PORT') ?: 5432,
                    'dbname' => $this->databaseName,
                ];
                break;
            default:
                throw new Exception('Desired Data Source not compatible with C11K\\Database');
        }

        return $this->source . $this->buildConnectionString($attributes);
    }

    /**
     * @param array $attributes
     * @return string
     */
    private function buildConnectionString(array $attributes): string
    {
        $dsnArray = [];
        foreach ($attributes as $attribute => $value) {
            $dsnArray[] = $attribute . '=' . $value;
        }

        return implode(';', $dsnArray);
    }

    /**
     * Prepares a statement for execution.
     * Alias of `prepare`
     *
     * @param string $query
     */
    public function query(string $query)
    {
        $this->prepare($query);
    }

    /**
     * Prepares a statement for execution.
     *
     * @param string $query
     */
    public function prepare(string $query)
    {
        $this->statement = $this->dbHandle->prepare($query);
    }

    /**
     * Returns a single row from the result set. Use this when you expect the result set to only contain 1 row.
     * This will automatically execute the query before returning the row.
     *
     * @param int $fetch_style
     * @return mixed : Array for successful result, false for no result
     */
    public function single(int $fetch_style = PDO::FETCH_ASSOC)
    {
        $this->execute();

        return $this->statement->fetch($fetch_style);
    }

    /**
     * Executes the prepared statement.
     *
     * @param array|null $parameters
     * @return bool
     */
    public function execute(?array $parameters = null): bool
    {
        if (is_array($parameters)) {
            return $this->statement->execute($parameters);
        }

        return $this->statement->execute();
    }

    /**
     * @return bool
     */
    public function beginTransaction(): bool
    {
        return $this->dbHandle->beginTransaction();
    }

    /**
     * @return bool
     */
    public function endTransaction(): bool
    {
        return $this->dbHandle->commit();
    }

    /**
     * @return bool
     */
    public function cancelTransaction(): bool
    {
        return $this->dbHandle->rollBack();
    }

    /**
     * @return string
     */
    public function debugDumpParams(): string
    {
        ob_start();
        $this->statement->debugDumpParams();
        $debugDumpParams = ob_get_contents();
        ob_end_clean();

        return $debugDumpParams;
    }

    /**
     * Quotes a string for use in a query.
     *
     * Replacement to mysqli_real_escape_string()
     *
     * @param mixed $stringToBeEscaped
     * @return string
     */
    public function realEscapeString($stringToBeEscaped): string
    {
        return $this->dbHandle->quote('' . $stringToBeEscaped, $this->getDataType($stringToBeEscaped));
    }

    /**
     * @param mixed $parameter
     * @return int
     */
    private function getDataType($parameter): int
    {
        switch (true) {
            case is_int($parameter):
                $type = PDO::PARAM_INT;
                break;
            case is_bool($parameter):
                $type = PDO::PARAM_BOOL;
                break;
            case is_null($parameter):
                $type = PDO::PARAM_NULL;
                break;
            default:
                $type = PDO::PARAM_STR;
        }

        return $type;
    }

    /**
     * Returns an array indexed by column number as returned in your result set, starting at column 0.
     *
     * Replacement to mysqli_fetch_array()
     *
     */
    public function fetchArray()
    {
        return $this->statement->fetch(PDO::FETCH_NUM);
    }

    /**
     * Returns an array indexed by column name as returned in your result set.
     *
     * Replacement to mysqli_fetch_assoc().
     *
     * @return array
     */
    public function fetchAssoc(): array
    {
        return $this->statement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Equivalent to mysqli_free_result(). Releases results still in memory. This is *REQUIRED* if you do not consume
     * the entire result set before calling another query.
     *
     * @return bool
     */
    public function freeResult(): bool
    {
        return $this->statement->closeCursor();
    }

    /**
     * Returns the number of rows affected by the last DELETE, INSERT, or UPDATE statement.
     *
     * @return int
     */
    public function rowCount(): int
    {
        return $this->statement->rowCount();
    }

    /**
     * @return string
     */
    public function error(): string
    {
        $error = $this->error;
        $this->error = null;

        return '' . $error;
    }

    /**
     * Returns the ID of the last inserted row or sequence value.
     * This method may not return a meaningful or consistent result across different PDO drivers.
     *
     * Replacement to mysqli_insert_id(). Alias of lastInsertId().
     *
     * @param string|null $name Name of the sequence object from which the ID should be returned.
     * @return string
     */
    public function insertID(?string $name = null): string
    {
        return $this->lastInsertId($name);
    }

    /**
     * @param string|null $name Name of the sequence object from which the ID should be returned.
     * @return string
     */
    public function lastInsertId(?string $name = null): string
    {
        return $this->dbHandle->lastInsertId($name);
    }

    /**
     * Replacement to sending mysqli_query($query) for SQL Commands that do not return results (INSERT, DELETE, etc.)
     *
     * @param string $query
     * @return int
     */
    public function rawQuery(string $query): int
    {
        return $this->dbHandle->exec($query);
    }

    /**
     * @param mixed $pdoAttribute
     * @return mixed
     */
    public function getAttribute($pdoAttribute)
    {
        return $this->dbHandle->getAttribute($pdoAttribute);
    }

    /**
     * Returns the number of rows in the return set.
     *
     * Replacement to mysqli_num_rows($result)
     *
     * @return int
     */
    public function numRows(): int
    {
        $rows = $this->statement->fetchAll(PDO::FETCH_ASSOC);
        $this->resultSet();
        $count = count($rows);
        $this->statement->execute();

        return $count;
    }

    /**
     * Returns an array containing all the remaining rows in the result set. Defaults to \PDO::FETCH_ASSOC, but can be
     * changed to a different fetch_style by sending it as the parameter. This will auto-execute the query before
     * returning the result set.
     *
     * @param int $fetchStyle \PDO::FETCH_ASSOC
     * @return array
     */
    public function resultSet(int $fetchStyle = PDO::FETCH_ASSOC): array
    {
        $this->execute();

        return $this->statement->fetchAll($fetchStyle);
    }

    /**
     * @param array $parametersArray
     * @return bool
     */
    public function bindAndExecute(array $parametersArray): bool
    {
        $this->bindArray($parametersArray);

        return $this->execute();
    }

    /**
     * Binds an array to the query where the array keys are parameters, and the array values are the values.
     *
     * @param array $array [':parameter1' => 'value1', ':parameter2' => 'value2' ...]
     */
    public function bindArray(array $array)
    {
        foreach ($array as $parameter => $value) {
            $this->bind($parameter, $value, $this->getDataType($value));
        }
    }

    /**
     * Binds a value to a parameter.
     *
     * @param mixed $parameter
     * @param mixed $value
     * @param mixed $type \PDO::PARAM_[INT|BOOL|NULL|STR]
     */
    public function bind($parameter, $value, $type = null)
    {
        if (is_null($type)) {
            $type = $this->getDataType($value);
        }

        $this->statement->bindValue($parameter, $value, $type);
    }
}
