<?php

namespace C11K\Pdo\Tests;

use C11K\Pdo\Database;
use PHPUnit\Framework\TestCase;

class DatabaseTest extends TestCase
{
    private $chbs = 'correct horse battery staple';

    /**
     * @test
     */
    public function connectsToStore()
    {
        $database = new Database();
        $this->assertInstanceOf(Database::class, $database);
        $this->assertEquals('sqlite', $database->getAttribute(\PDO::ATTR_DRIVER_NAME));

        return $database;
    }

    /**
     * @test
     * @depends connectsToStore
     * @param Database $database
     * @return Database
     */
    public function canCreateTable(Database $database)
    {
        $sql = 'DROP TABLE IF EXISTS testingTable';
        $database->rawQuery($sql);

        $sql = 'CREATE TABLE testingTable (
        `id` INT(11) PRIMARY KEY,
`some_id` INT(11) NOT NULL,
`astring` VARCHAR(200) NULL,
`anotherstring` VARCHAR(200) NULL
);';
        $database->query($sql);
        $database->execute();

        $sql = "INSERT INTO testingTable (id, some_id, astring, anotherstring) VALUES (:id, :some_id, :astring, :anotherstring)";
        $database->query($sql);
        $database->bind(':id', 1);
        $database->bind(':some_id', '10');
        $database->bind(':astring', $this->chbs);
        $database->bind(':anotherstring', 'Tr0ub4dor&3');
        $database->execute();

        $sql = "SELECT * FROM testingTable ORDER BY id";
        $database->query($sql);
        $row = $database->single();
        $this->assertEquals($this->chbs, $row['astring']);

        return $database;
    }

    /**
     * @test
     * @depends canCreateTable
     * @param Database $database
     * @return Database
     */
    public function boolAndNullBindingAsArray(Database $database)
    {

        $sql = "INSERT INTO testingTable (id, some_id, astring, anotherstring) VALUES (:id, :some_id, :astring, :anotherstring)";
        $database->query($sql);
        $database->bindArray([
            ':id' => 2,
            ':some_id' => 8675309,
            ':astring' => null,
            ':anotherstring' => true,
        ]);
        $database->execute();

        $database->execute([
            ':id' => 3,
            ':some_id' => 9786410,
            ':astring' => 'Bacon ipsum dolor amet picanha shoulder landjaeger t-bone corned beef shank.',
            ':anotherstring' => 'bacon',
        ]);

        $database->bindArray([
            ':id' => 4,
            ':some_id' => 9786410,
            ':astring' => 'Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro.',
            ':anotherstring' => 'zombie',
        ]);
        $database->execute();
        $this->assertEquals(1, $database->rowCount());

        $database->query('SELECT * FROM testingTable ORDER BY id');
        $rows = $database->resultSet();
        foreach ($rows as &$row) {
            $this->assertNotEmpty($row['id']);
        }

        // 2017-08-18 TIL function names are case-insensitive.
        $rows = $database->resultSet(\PDO::FETCH_NUM);
        foreach ($rows as &$row) {
            // Columns 2 & 3 can be null, so don't test against these columns!!
            $this->assertNotEmpty($row[0]);
        }

        $sql = "SELECT * FROM testingTable WHERE id = :id";
        $database->query($sql);
        $database->bind(':id', 4);
        $row = $database->single();
        $this->assertEquals(
            'Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro.',
            $row['astring']
        );

        $this->assertEquals(4, $database->lastInsertId());

        return $database;
    }

    /**
     * @test
     * @depends boolAndNullBindingAsArray
     * @param Database $database
     * @return Database
     */
    public function escapeQueryAndTransactions(Database $database)
    {
        $numeric = $database->realEscapeString(1);
        $this->assertEquals("'1'", $numeric);

        $boolean = $database->realEscapeString(true);
        $this->assertEquals("'1'", $boolean);

        $nullval = $database->realEscapeString(null);
        $this->assertEquals("''", $nullval);

        $stringval = $database->realEscapeString("Chickens don't clap!");
        $this->assertEquals("'Chickens don''t clap!'", $stringval);

        $chbs = $database->realEscapeString($this->chbs);
        $database->beginTransaction();
        $database->rawQuery("INSERT INTO testingTable (id, some_id, astring) VALUES (5, 5, " . $chbs . ")");
        $database->cancelTransaction();

        $database->beginTransaction();
        $query = "INSERT INTO testingTable (id, some_id, astring) VALUES (5, " . $numeric . ", " . $stringval . ")";
        $database->rawQuery($query);
        $this->assertEquals(5, $database->insertID());
        $database->endTransaction();

        $sql = "SELECT astring FROM testingTable WHERE id = ?";
        $database->query($sql);
        $database->bindArray([1 => 5]);
        $row = $database->single();
        $this->assertEquals("Chickens don't clap!", $row['astring']);

        return $database;
    }

    /**
     * @test
     * @depends escapeQueryAndTransactions
     * @param Database $database
     * @return Database
     */
    public function fetchArrayAssoc(Database $database)
    {
        $sql = "SELECT * FROM testingTable";
        $database->query($sql);
        $database->execute();
        $this->assertEquals(5, $database->numRows());
        while ($row = $database->fetchArray()) {
            if ($row[0] == 4) {
                $database->freeResult();
                break;
            }
            $doStuff[] = implode("!", $row);
        }

        unset($row);
        $database->query($sql);
        $this->assertEmpty($database->error());

        $debug = $database->debugDumpParams();
        $this->assertContains($sql, $debug);

        return $database;
    }

    /**
     * @test
     * @depends fetchArrayAssoc
     * @param Database $database
     * @expectedException \PDOException
     */
    public function badSQLThrowsException(Database $database)
    {
        $sql = 'SELECT * FROM testTabel WHERE id= :id';

        $database->query($sql);
        $error = $database->error();
    }

    /**
     * @test
     * @depends fetchArrayAssoc
     * @param Database $database
     */
    public function bindAndExecute(Database $database)
    {
        $query = "INSERT INTO testingTable(id, some_id, astring) VALUES(:id, :some_id, :astring)";
        $database->query($query);
        $database->bindAndExecute([':id' => 6, ':some_id' => 42, ':astring' => 'No, Pop-pop does not get a treat.']);
        $this->assertEquals(1, $database->rowCount());

        $database->bindAndExecute(
            [
                ':id' => 7,
                ':some_id' => 14,
                ':astring' => 'I night, the stars were shining; II hearts were intertwining, III words',
            ]
        );
        $this->assertEquals(1, $database->rowCount());
    }

    /**
     * @test
     * @depends fetchArrayAssoc
     * @param Database $database
     * @return Database
     */
    public function noResults(Database $database): Database
    {
        $query = "SELECT id, astring FROM testingTable WHERE id = :id";
        $database->prepare($query);
        $database->bind(':id', 938362047);
        $row = $database->single();

        $this->assertFalse($row);

        return $database;
    }

    /**
     * @test
     * @depends noResults
     * @param Database $database
     */
    public function fetchOneAtATime(Database $database)
    {
        $query = "SELECT * FROM testingTable ORDER BY id";
        $doStuff = [];
        $database->prepare($query);
        $database->execute();

        while ($row = $database->fetchAssoc()) {
            if ($row['id'] == 4) {
                $database->freeResult();
                break;
            }
            $doStuff[] = implode("!", $row);
        }
        $this->assertNotEmpty($doStuff);
        $this->assertEquals(3, count($doStuff));
    }

}
